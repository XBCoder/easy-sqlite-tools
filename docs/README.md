# 数据库代码相关说明

## database文件

该文件主要代码为`DataBase`类，`DevelopmentTest`。

注意：`DataBase`类会在`config.py`中生成一个名为`DB`的默认数据库类。

## 方法讲解

#### **构造**
```python
def __init__(self, db_file, table_modules, not_warning=False) -> None: pass
```
参数：
 - 1. `db_file`: 数据库在磁盘上的保存路径。
 - 2. `table_modules`: 保存表的模块，对于如何创建表，参考`sqlite_utils说明.md`; 注意：`config.py`中默认使用`database/tables.py`和`database.playground.py`两个模块初始化默认数据库。
 - 3. `not_warning`: 是否在控制台中输出警告日志，只有当某些方法中发生了异常时，该选项才会有用。

####  **析构**
```python
def __del__(self): pass
```
该函数会在类将被销毁时调用，该方法内主要是等待所有线程结束，并提交数据并关闭数据库。


#### **警告修饰器**
```python
def warning(func): pass
def warning_exec_sql(func): pass
```
当某些类方法中可能会出现异常，并且不希望异常使得程序终止时，可以在方法上加上这个修饰器，其内部构造为try语句内调用函数，当捕获到异常后根据not_warning变量判断进行打印输出其中。

其中`warning_exec_sql`修饰器专门针对`exec_sql`这个方法(针对线程)

#### **初始化检查**
```python
def _check_table(self): pass
```
该函数会在类构造函数内部的合适位置被调用，目的是根据构造参数`table_modules`对比本地文件，判断哪些表需要删除，哪些表需要添加，哪些表需要更新，最终使得本地数据库与`table_modules`中包含的表一致

**注意**：该函数内部可能会抛出异常，异常主要发生在更新表上，但是但异常发生时，方法内可以保证不对表进行更新，即不会破坏本地数据库

#### **处理表模块**
```python
def _process_tables(table_modules): pass
```
用于对构造函数中的`table_modules`进行处理并读取出所有的表

#### **SQL语句执行**
```python
def _exec_sql(self, sql, hash): pass
def _exec_sql_no_commit(self, sql, hash): pass
```
这两个内部函数的用途是执行一条sql语句，两个函数都是线程函数，其主要区别为：
 - `_exec_sql`: 该函数执行后立刻commit，适用于小规模修改数 ( 对应方法`insert_table` )
  - `_exec_sql_no_commit`: 该函数执行后暂时不做commit，在所有语句执行完后进行commit，该函数用于配合`Inserter`执行大规模数据修改

#### **小规模数据插入**
```python
def insert_table(self, table, *data, **kw_data): pass
```
该函数会调用上述的`_exec_sql`向数据库中插入一条数据

参数：
 - `table`: 要插入数据的表，可传入的类型为：
   - 1. `TableMeta`数据(参见`sqlite_utils说明.md`)
   - 2. 字符串数据，其值为表名
 - `data`: 位置参数，为要插入的数据，数据传入方式：
   - 1. 直接把数据放入参数括号内，接在`table`参数后即可，要求是需传入表中的所有属性对应的数据
   - 2. 一个字典(`dict`)，其格式稍后讲解
 - `kw_data`: 关键字参数，当使用该种调用方式时，要求不能包含除`table`以外的位置参数，数据传入方式：
   - 直接以`attr_name=attr_data`的方式传入

对于`data`中传入字典的情况：

 - 字典的键`key`，可以是定义TableMeta表的属性，也可以是字符串，值为属性名
 - 字典的值`value`，即要插入的数据

举例：
```python
# 假设有一个TableMeta表类：
class CarType(sq.SqTable, metaclass=sq.TableMeta):
    """此表用于存储车型信息"""
    Name = sq.TEXT_Sqlite  # 车型名称
    Idx = (sq.INT_Sqlite, not None)  # 车型编号

    def __init__(self) -> None:
        self.PRIMARY = [self.Idx]


# 1.  使用data的方法1插入数据：
DataBase.insert_table(CarType, None, 1)
# 要求表中所有的属性都要传值，对于某些可空属性需要手动传入None，并且传值顺序要与表中对应一致


# 2.  使用data的方法2插入数据：
insert_data = {
    CarType.Idx: 2,
    'Name': '奔驰'
}
DataBase.insert_table(CarType, None, 1)
# 对于数据顺序无要求，可以只插入表的部分属性(不插入的属性要求可空)


# 3.  使用kw_data插入数据
DataBase.insert_table(CarType, Idx=3, Name=None)
# 同理，对于数据顺序无要求，可以只插入表的部分属性(不插入的属性要求可空)，但是需要记忆表中列的名称
```

#### **添加数据上下文管理器**
```python
@property
def inserter(self): pass
```
该方法被修饰为属性，其返回一个名为`_inserter`上下文管理器类，使用该方法可实现大量数据的插入

**注意：**由于该方法返回一个类的实例，因此需要对其构造，其参数为上方`insert_table`中的`table`，即要修改的表，可传入的类型与`insert_table`相同

同时该类还包含一个插入方法`insert_table`：
```python
def insert_table(this, *data, **kw_data): pass
```
其使用方法与上文中的`insert_table`基本一致，除了少了`table`参数

使用方法：
```python
# 类似python内的open方法
data = [···]

with DB.(CarType) as inserter:
    for carName, carType in data:
        inserter.insert_table(carName, carType)
        # 此处只演示了一种插入数据的传入方法
        # 但是和上文的insert_table一样，其他两种方法也可以使用
```

#### **数据查询**
```python
def select_table(self, tables, attrs, where: str = '', groupby=None, orderby=None, join_method=JOIN_METHOD.NATURAL): pass
# 查询函数
# 参数1：是要在哪些表中查询数据，可以提供单个表，也可以提供多个表，可接受的数据类型：(str, list, tuple, TableMeta为元类的表)
# 参数2：要查询的属性，可接受的值：('*', str, TYPE_Sqlite, list, tuple)
# 参数3：where值，传入一个字符串, 
# 参数4：分组信息，可接受的值：(str, TYPE_Sqlite, list, tuple)
# 参数5：排序设置，可接受的值：(str, TYPE_Sqlite, list, tuple)
# 参数6：多表时的连接方式，可参考常数类sqliteunits.JOIN_METHOD
# 函数返回一个List，为查询结果
```
参数：
 - 1. `tables`: 要在哪些表中查询数据，可以提供单个表，也可以提供多个表，可接受的数据类型：(`str`, `TableMeta`表, 以及由前两者构成的`list`, `tuple`)
 - 2. `attrs`: 要查询的属性，可接受的值：(`str`, `TYPE_Sqlite`, 由前两者构成的`list`, `tuple`, 以及`'*'`)
 - 3. `where`: 为一个`str`，表示查询的`WHERE`语句(没想到其他更好的方式所以只能搞`str`)如：`tab1.attr1=tab2.attr2`
 - 4. `groupby`: 分组信息，可接受的值：(`str`, `TYPE_Sqlite`, 以及由前两者构成的`list`, `tuple`)
 - 5. `orderby`: 排序设置，可接受的值：(`str`, `TYPE_Sqlite`, 以及由前两者构成的`list`, `tuple`)
 - 6. `join_method`: 多表查询时的连接方式，可参考常数类`sqliteunits`.`JOIN_METHOD`

返回：`list`，为查询结果

使用说明：
```python
# 1. 查询单表所有数据
res = DB.select_table(tables=carName, attrs='*')


# 2. 带条件查询单表所有数据
where_str = f'{carName.Idx}=1'
res = DB.select_table(tables=carName, attrs='*', where=where_str)


# 3. 对结果排序
res = DB.select_table(tables=carName, attrs='*', orderby=carName.Idx.DESC_ORDER)  # 参考sqlite_utils说明.md

# 4. 查询统计结果(假设PERSON表的age表示年龄，gender表示性别)
# 该查询为查询不同性别人的平均年龄
res = DB.select_table(tables=PERSON, attrs=PERSON.age.AVG, groupby=Person.gender)  # 参考sqlite_utils说明.md

# 5. 多表查询
# 5.1 自然连接(假设A，B拥有同名属性)
res = DB.select_table(tables=(A, B), attrs=(A.a, 'B.b'))
# 5.2 其他连接，连接方式查看sqlite_utils.JOIN_METHOD.XXX
where_str = f'{A.c}={B.d}'
res = DB.select_table(tables=(A, B), attrs=(A.a, 'B.b'), where=where_str,
      join_method=JOIN_METHOD.CROSS)
```

#### **清空某个表**
```python
def clear_table(self, table): pass
```
`table`可以是值为表名的字符串，也可以是`TableMeta`类型的表