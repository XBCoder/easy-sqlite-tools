# 数据库代码相关说明

## **units文件**

这个文件的主旨提供了一种使用`python`简便创建`sqlite`表的方法：

说简单点就是用于数据库建表以及维护，将`sqlite`的表的创建转换为`python`类的编写

### **使用条件**

其中，可被转换为`sqlite table`的`python`类需要满足条件：

- 1 继承自类`SqTable`
- 2 将类`TableMeta`设置为`metaclass`
- 3 重载类方法`Constraint`，在该方法中设置表的约束

即可在编写完后调用成员函数`getCreateSQL`得到对应的sql语句

### **一个简单的例子**

该例子说明如何通过该方法创建一个表：
- 1 创建一个类，类名为我们要创建的表名，如`TABLE`，该类继承于`SqTable`，且将`metaclass`设置成`TableMeta`
- 2 在类中创建全局变量，每一个变量的变量名即为在表中的列名，如`ID`；全局变量初始化的等号右端，是关于该属性的设置：
  - 2.1 可以传入一个单独的`TYPE_Sqlite`值，表示该属性的类型为`TYPE`, 需要注意没有指定的情况下，属性默认可为空
  - 2.2 其他情况则需要传入一个`tuple`或`list`，该情况下其第一个元素必须为`TYPE_Sqlite`, 后面的修饰符顺序不做要求，可用的修饰符有：
    - 2.2.1 `None` / `not None`，表明该属性是否可以为空值。
    - 2.2.2 `AUTOINCREMENT_Sqlite`，表明该属性是一个自增的值。
    - 2.2.3 `DEFAULT(value)`，表明该属性的默认值为value。
- 3 创建完所有的属性后，接下来创建一个类函数：`Constraint(cls)`，该函数用于设置约束条件；父类`SqTable`定义了该函数以及三种约束：`PRIMARY`，`UNIQUE`，`FOREIGN`；表示主键约束，唯一约束以及外键约束。
  - 3.1 对于`PRIMARY`，在`Constraint`函数中，将`PRIMARY`设置为要设置为主键的属性组成的列表或元组，在下面的例子中可以看到。
  - 3.2 对于`UNIQUE`也是同理，操作与3.1中的描述一致。
  - 3.3 对于`FOREIGN`则要特殊很多，对于每一个外键对，在设置时表现为一个2元组，第一个元素为本表中要引用其他属性的列，第二个元素为被引用的其他表的属性，更详细的写法可以在下面的例子中看出。

## **例子说明**

在下面的例子中，属性`ID`被设置为INT类型，且令其自增；`NAME`则被设置为非空的TEXT属性，`MONEY`是一个实数属性

在`Constraint`函数中，`ID`属性被设置为主键，`NAME`属性被设置为唯一，`NAME`属性被设置为`OTHERS_TABLE.ATTR`的外键
```python
class TABLE(SqTable, metaclass=TableMeta):
    '''
    继承自类SqTable，并将TableMeta设置为metaclass
    其中表中的每个属性其类型为TYPE_Sqlite
    '''
    # 表中的每一行为一个全局变量声明
    ID = (INT_Sqlite, AUTOINCREMENT_Sqlite)
    # 一个自动增加的INT型数据，稍后会设置其为主键

    NAME = (TEXT_Sqlite, not None)
    # 一个非空的文本型数据，表示名字

    MONEY = REAL_Sqlite
    # 一个实数型数据

    @classmethod
    def Constraint(cls):
        cls.PRIMARY = [cls.ID]
        cls.UNIQUE = [cls.NAME]
        cls.FOREIGN = [(cls.NAME, OTHERS_TABLE.ATTR)]
```

再次提醒：`TYPE_Sqlite`为可使用的类型或约束，类型必须在元组中的第一个位置，可用的类型包括：`INT_Sqlite`，`REAL_Sqlite`，`TEXT_Sqlite`，`DATE_Sqlite`

将`TABLE`实例化并调用`getCreateSQL`后可得到类似的数据：
```sql
# 执行TABLE().getCreateSQL()后得到的输出

CREATE TABLE FuzzyKnowledge (
    ID INT PRIMARY KEY AUTOINCREMENT ,
    NAME TEXT NOT NULL ,
    MONEY REAL ,
    FOREIGN KEY(NAME) REFERENCES OTHERS_TABLE(ATTR),
    UNIQUE(NAME)
);
```

    提示: 对于类class TABLE(SqTable, metaclass=TableMeta)来说，type(TABLE)为TableMeta，而type(TABLE())【即实例化以后】其类型为SqTable

## **其他介绍**

## 表更新

#### **表更新讲解**
由于继承`SqTable`类创建的表，最终会交予`sq_database.py`中的`DataBase`类处理，其中在`DataBase`初始化时，会进行一个检查操作：对比本地数据库与提供的`SqTable`类创建的表——将多余的表删除，将缺少的表创建，而对于共同存在的表，若表的列不相同，则需要进行更新操作。

`SqTable`中定义了一个参数：`UPDATE_METHOD`，而`sqlite_utils`中定义了一个常数类：`sqlite_utils.UPDATE_METHOD`，`SqTable.UPDATE_METHOD`的默认值为`UPDATE_METHOD.CHANGE_MERGE`。

下面讲解一下不同更新方式的区别：`sqlite_utils.UPDATE_METHOD`包含3个值：`UPDATE_METHOD.CHANGE_MERGE`，`UPDATE_METHOD.CHANGE_CLEAR`，`UPDATE_METHOD.CHANGE_RENAME`
 - 1 `UPDATE_METHOD.CHANGE_MERGE`：作为默认值，其作用是在检查到本地表与最新表的定义冲突时，尽可能保存数据，将新旧两表的共同属性保留。
 - 2 `UPDATE_METHOD.CHANGE_CLEAR`：是最简单的处理，当检测到差异后，删除本地的表，从最新的定义创建新的表。
 - 3 `UPDATE_METHOD.CHANGE_RENAME`：有的时候，我们仅仅只是将表中的列名修改，并不希望改动表内的数据，此时这个方法就派上了用场。

#### **如何使用**

只需要在TABLE的定义中增加：`UPDATE_METHOD = sq.UPDATE_METHOD.CHANGE_RENAME`，如：
```python
class TABLE(SqTable, metaclass=TableMeta):
    UPDATE_METHOD = sq.UPDATE_METHOD.CHANGE_RENAME

    # ...
```

## 表属性方法
在使用`SELETE`查询时，有时会需要查询某属性的统计值，`SqTable`中定义了属性`COUNT`, `MAX`, `MIN`, `AVG`, `ASC_ORDER`, `DESC_ORDER`; 且这些属性间支持连锁调用：
```python
print(TABLE.NAME)
# 可以使用print(TABLE.ATTR)查看属性，如：
# 输出：TABLE.NAME

print(TABLE.NAME.AVG)
# 单次调用
# 输出：AVG(TABLE.NAME)

print(TABLE.NAME.MIN.COUNT)
# 连锁调用
# 结果为：COUNT(MIN(TABLE.NAME))
```