from setuptools import setup

setup(
    name = "ease_sqlite",
    version = "v1.0.5",
    packages = ["ease_sqlite"],
    description='一个简单的sqlite工具，可以简单的建表以及做sql操作',
    url='https://gitee.com/XBCoder/easy-sqlite-tools',
    keywords='sqlite tools easy',
    project_urls={
        "Documentation": "https://gitee.com/XBCoder/easy-sqlite-tools",
        "Source Code": "https://gitee.com/XBCoder/easy-sqlite-tools",
    },
)